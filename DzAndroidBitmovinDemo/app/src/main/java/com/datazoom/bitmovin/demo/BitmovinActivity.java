package com.datazoom.bitmovin.demo;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.bitmovin.player.BitmovinPlayer;
import com.bitmovin.player.config.media.SourceConfiguration;
import com.datazoom.bitmovin.BitmovinPlayerCollector;
import com.datazoom.bitmovin.demo.databinding.ActivityBitmovinBinding;
import com.dz.collector.android.collector.DZEventCollector;
import com.dz.collector.android.connectionmanager.DZBeaconConnector;
import com.dz.collector.android.model.DatazoomConfig;
import com.dz.collector.android.model.Event;

import org.json.JSONArray;
import org.json.JSONException;

public class BitmovinActivity extends AppCompatActivity {
    private static final String TAG = "BitmovinActivity";

    private String stagingUrl = "https://stagingplatform.datazoom.io/beacon/v1/";
    private String productionUrl = "https://platform.datazoom.io/beacon/v1/";
    private String url;
    private boolean mediaPlayerConfigured;
    private boolean sdkInitialized;

    private BitmovinPlayer bitmovinPlayer;
    private SourceConfiguration sourceConfiguration;

    private ActivityBitmovinBinding activityMainBinding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_bitmovin);

        initVideoTypeSpinner();
        activityMainBinding.initilaizeSDKSwitch.setOnCheckedChangeListener((compoundButton, checked) -> {
            if (checked && !mediaPlayerConfigured) {
                configureSDK();
            }
        });

        activityMainBinding.btnSubmit.setOnClickListener(v -> {
            if (!sdkInitialized) {
                Toast.makeText(this, " SDK is not initialized", Toast.LENGTH_LONG).show();
            } else {
                configureMediaPlayer();
            }
        });

        activityMainBinding.qaButton.setOnClickListener(view -> {
            activityMainBinding.qaButton.setBackgroundResource(R.drawable.rounded_background_white_with_border);
            activityMainBinding.productionButton.setBackgroundResource(R.drawable.round_white_background);
            activityMainBinding.connectingToUrlValue.setText(stagingUrl);
        });

        activityMainBinding.productionButton.setOnClickListener(view -> {
            activityMainBinding.productionButton.setBackgroundResource(R.drawable.rounded_background_white_with_border);
            activityMainBinding.qaButton.setBackgroundResource(R.drawable.round_white_background);
            activityMainBinding.connectingToUrlValue.setText(productionUrl);
        });

        activityMainBinding.closeVideoButton.setOnClickListener(view -> {
            if (bitmovinPlayer != null) {
                bitmovinPlayer.pause();
                activityMainBinding.videoTypeSpinner.setEnabled(true);
                activityMainBinding.closeVideoButton.setVisibility(View.GONE);
                activityMainBinding.bitmovinPlayerView.setVisibility(View.INVISIBLE);
                activityMainBinding.settingsContainer.setVisibility(View.VISIBLE);
                activityMainBinding.btnPush.setVisibility(View.GONE);
                activityMainBinding.btnSubmit.setVisibility(View.VISIBLE);
                activityMainBinding.btnSubmit.setEnabled(true);
            }
        });

        activityMainBinding.buildNumebrConatinner.setOnLongClickListener(view -> {
            Toast.makeText(BitmovinActivity.this, "Version number: " + BuildConfig.VERSION_NAME, Toast.LENGTH_LONG).show();
            return false;
        });

    }


    private void configureMediaPlayer() {
        if (bitmovinPlayer == null) {
            bitmovinPlayer = activityMainBinding.bitmovinPlayerView.getPlayer();
        }
        activityMainBinding.btnPush.setVisibility(View.VISIBLE);
        activityMainBinding.btnSubmit.setVisibility(View.GONE);
        activityMainBinding.settingsContainer.setVisibility(View.GONE);
        activityMainBinding.btnSubmit.setEnabled(false);
        activityMainBinding.bitmovinPlayerView.setVisibility(View.VISIBLE);
        mediaPlayerConfigured = true;
        activityMainBinding.closeVideoButton.setVisibility(View.VISIBLE);

        sourceConfiguration = new SourceConfiguration();
        sourceConfiguration.addSourceItem(url);
        bitmovinPlayer.load(sourceConfiguration);

    }

    private void configureSDK() {
        String configId = activityMainBinding.txtConfiguration.getText().toString();
        String configUrl = activityMainBinding.connectingToUrlValue.getText().toString();

        if (bitmovinPlayer == null) {
            bitmovinPlayer = activityMainBinding.bitmovinPlayerView.getPlayer();
        }
        BitmovinPlayerCollector.create(bitmovinPlayer, BitmovinActivity.this)
                .setConfig(new DatazoomConfig(configId, configUrl))
                .connect(new DZBeaconConnector.ConnectionListener() {
                    @Override
                    public void onSuccess(DZEventCollector dzEventCollector) {
                        sdkInitialized = true;
                        setupCustomMetadata();
                        setupCustomEvent(dzEventCollector);
                    }

                    @Override
                    public void onError(Throwable t) {
                        showAlert("Error", "Error while creating NativeAndroidConnector, error:" + t.getMessage());
                    }
                });

    }

    private void showAlert(String title, String message) {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(this);
        }
        builder.setTitle(title)
                .setMessage(message)
                .setPositiveButton(android.R.string.yes, (dialog, which) -> {
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    private void setupCustomMetadata() {
        try {
            DZEventCollector.setCustomMetadata(new JSONArray(
                    "["
                            + "{\"custom_player_name\": \"Android Bitmovin Media Player\"},"
                            + "{\"custom_domain\": \"demo.datazoom.io\"}"
                            + "]"
            ));
        } catch (JSONException e) {
            Log.e(TAG, "Error setting custom metadata", e);
        }
    }

    private void setupCustomEvent(DZEventCollector dzEventCollector) {
        Event event = new Event("Datazoom_SDK_Loaded", new JSONArray());
        dzEventCollector.addCustomEvent(event);
    }

    private void initVideoTypeSpinner() {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.video_types));
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        activityMainBinding.videoTypeSpinner.setAdapter(adapter);
        // Default value
        url = getResources().getStringArray(R.array.video_urls)[0];
        activityMainBinding.videoTypeSpinner.setSelection(0, false);
        activityMainBinding.videoTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                url = getResources().getStringArray(R.array.video_urls)[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (bitmovinPlayer != null) {
            bitmovinPlayer.pause();
        }
    }
}